package ru.t1.aksenova.tm.api.сontroller;

import ru.t1.aksenova.tm.model.Project;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void showProject(Project project);

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectById();

    void removeProjectByIndex();

}
